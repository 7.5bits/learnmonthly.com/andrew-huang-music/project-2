#!/usr/bin/env bash

set -o errexit
set -o nounset

input_file=$1
start_time=$2
length=$3
volume_gain=$4
output_file=$5

ffmpeg -ss $start_time -t $length -i $input_file -af "pan=mono|c0=FL,volume=${volume_gain}dB" $output_file

