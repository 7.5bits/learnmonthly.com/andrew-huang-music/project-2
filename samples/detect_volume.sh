#!/usr/bin/env bash

set -o errexit
set -o nounset

input_file=$1

ffmpeg -i $input_file -af "pan=mono|c0=FL,volumedetect" -vn -sn -dn -f null /dev/null 2>&1 | grep -o 'max_volume:.*'

